import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import Layout from './components/Layout/Layout';
import ProductsListBuilder from './containers/ProductsList/ProductsListBuilder/ProductsListBuilder';

function App() {
  return (
    <BrowserRouter>
      <div>
        <Layout>
          <ProductsListBuilder />
        </Layout>
      </div>
    </BrowserRouter>
  );
}

export default App;
