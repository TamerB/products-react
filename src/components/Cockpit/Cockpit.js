import React from 'react';
import { Link } from 'react-router-dom';

import classes from './Cockpit.module.css';
import Aux from '../../hoc/Aux';

const cockpit = (props) => {
  return (
    <Aux>
      <div className={classes.Container}>
        <div className={classes.Wrapper}>
          <div className={classes.Title}>
            <h2>{props.showWhich}</h2>
          </div>
        </div>
        <div className={classes.Wrapper}>
          {Object.entries(props.details).map((arr) => ((arr[0] === 'discount' || arr[0] === 'active') ? <div className={classes.filter}>
            <h5>{arr[0]}: {arr[0] === 'active' ? (arr[1] ? "Active" : "Inactive") : arr[1]}</h5>
          </div> : null))}
        </div>
        <div className={classes.Wrapper}>
          <div className={classes.filterTitle}>
            <h4>Filter By: </h4>
          </div>
          <div className={classes.filter}>
            <label>Department</label>
            <select name="departments" onChange={(event) => props.changeValue(event.target.value, 'department')} required >
            {Object.keys(props.department).length > 0 ? null : <option value=""></option>}
            {Object.keys(props.departments).map((key, value) => (<option value={key}>{props.departments[key].name}</option>))}
            </select>
          </div>

          <div className={classes.filter}>
            <label>Promotion</label>
            <input name="promotion" type="text" value={props.promotion.code} onChange={(event) => props.changeValue(event.target.value, 'promotion') } />
          </div>
        </div>
        <div className={classes.Wrapper}>
          <div className={classes.filterTitle}>
            <h4>Search By Product Name: </h4>
          </div>
          <div className={classes.filter}>
            <input name="search" type="text" onChange={(event) => props.getSearch(event.target.value) } />
          </div>
        </div>
        <div className={classes.Pagination}>
          <button onClick={() => props.updatePage(props.page > 1 ? props.page-1 : props.page)}>&laquo;</button>
          {Array.from({length: props.pages}, (v, k) => k+1).map((value, index) => 
            (<button onClick={() => props.updatePage(index+1)} className={props.page==index+1 ? classes.Active : ''}>{index+1}</button>)
          )}
          <button onClick={() => props.updatePage(props.page < props.pages ? props.page+1 : props.page)}>&raquo;</button>
        </div>
        {props.errorMessage ? <div className={classes.Wrapper}>
          <p className={classes.error}>{props.errorMessage}</p>
        </div>: null}
      </div>
    </Aux>
  );
};

export default cockpit;