import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import classes from './Product.module.css'

const product = (props) => {
  let department = props.departement;
  let promotion = props.promotion
  let promotions = '';
  props.promotions.map((p) => { promotions+= (p.active ? (' ' + p.code) : '')});
  let item = <div className={classes.Card}>
    <div className={classes.Description}>
      <div className={classes.Block}>
        <h5>{props.name}</h5>
      </div>
      <div className={classes.Block}>
        <p>${props.price}</p>
        {props.promotions.map((p) => (p.active ? <p>{'$' + (parseInt(props.price) - (parseInt(props.price) * parseInt(p.discount) / 100)) + ' - (' + p.discount + '%)'}</p> : ''))}
      </div>
      <div className={classes.Block}>
        <p>{props.department.name}</p>
      </div>
      <div className={classes.Block}>
        <p>{promotions}</p>
      </div>
    </div>
  </div>;

  return item;
};

export default product; 