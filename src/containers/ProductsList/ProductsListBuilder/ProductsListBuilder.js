import React, { useState, useEffect } from 'react';
import { Route, Switch, useHistory } from 'react-router-dom';

import Products from '../Products/Products';

import Aux from '../../../hoc/Aux';
import Cockpit from '../../../components/Cockpit/Cockpit';
import axios from '../../../axios-orders';
import Spinner from '../../../components/Spinner/Spinner';

function ProductsListBuilder () {
  const [errorMessage, setErrorMessage] = useState('');
  const [showWhich, setShowWhich] = useState('Products');
  const [display, setDisplay] = useState(<Spinner />);
  const [departments, setDepartments] = useState([]);
  const [promotions, setPromotions] = useState([]);
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [department, setDepartment] = useState('');
  const [promotion, setPromotion] = useState('');
  const [details, setDetails] = useState({});
  const [pages, setPages] = useState(1);
  const [page, setPage] = useState(1);
  const [currentFunction, setCurrentFunction] = useState('getProducts');

  const history = useHistory();


  useEffect(() => {
    setLoading(true);
    getDepartments();
    getPromotions();
    getProducts(page);
    getPages('/pages/');
  }, []);

  const getPages = (value) => {
    setLoading(true);
    axios.get(value)
      .then(response => {
        setPages(parseInt(response.data) || 1);
        setPage(1);
        setErrorMessage('');
      })
      .catch(error => {
        setErrorMessage('Something went wrong getting Number of pages');
        setLoading(false);
      });
  }

  const updatePage = (value) => {
    setPage(value);
    eval(currentFunction)(value);
  }

  const getDepartments = () => {
    setLoading(true);
    axios.get('/departments')
      .then(response => {
        setDepartments(response.data || []);
        setErrorMessage('');
      })
      .catch(error => {
        setErrorMessage('Something went wrong getting departments');
        setLoading(false);
      });
  }

  const getPromotions = () => {
    axios.get('/promotions')
      .then(response => {
        let i = response.data;
        setPromotions(i);
        setErrorMessage('');
      })
      .catch(error => {
        setErrorMessage('Something went wrong getting promotions');
        setLoading(false);
      });
  }

  const getProducts = (value) => {
    setLoading(true);
    setCurrentFunction('getProducts');
    setShowWhich('Products');
    setDetails({});
    axios.get('/products?page=' + value)
      .then(response => {
        setProducts(response.data || []);
        setErrorMessage('');
        setLoading(false);
      })
      .catch(error => {
        setErrorMessage('Something went wrong getting products');
        setLoading(false);
      });
  }

  const getDepartmentProducts = (value) => {
    setLoading(true);
    setCurrentFunction('getDepartmentProducts');
    axios.get('/departments/' + value + '/products?page=' + page)
      .then(response => {
        setProducts(response.data || []);
        setPages(response.data.count)
        setErrorMessage('');
        setLoading(false);
      })
      .catch(error => {
        setErrorMessage('Something went wrong getting products');
        setLoading(false);
      });
  }

  const getPromotionProducts = (value) => {
    setLoading(true);
    setCurrentFunction('getPromotionProducts');
    axios.get('/promotions/' + value + '/products?page=' + page)
      .then(response => {
        setProducts(response.data || []);
        setPages(response.data.count);
        setErrorMessage('');
        setLoading(false);
      })
      .catch(error => {
        setErrorMessage('Something went wrong getting products');
        setLoading(false);
      });
  }

  const changeValue = (value, tagType) => {
    if (tagType === 'department') {
      if (value !== '') {
        let d = departments[Number(value)];
        setDepartment(d || {});
        setPage(1);
        getDepartmentProducts(d.id);
        getPages('/dep_pages/' + d.id);
        setPromotion('');
        setShowWhich('Department: ' + d.name);
        setDetails({});
      } else {
        getProducts(page);
      }
    } else {
      if (value !== '') {
        let promotionObject = promotions.find(e => e.code === value);
        setPromotion(promotionObject || {});
        if (promotionObject == null) {
          setProducts([]);
        } else {
          getPromotionProducts(promotionObject.id);
          getPages('/pro_pages/' + promotionObject.id);
          setPage(1);
          setDepartment('');
          setShowWhich('Promotion: ' + promotionObject.code);
          setDetails(promotionObject);
        }
      } else {
        getProducts(page);
      }
    }
  }

  const getSearch = (value) => {
    setShowWhich('Products');
    setDetails({});
    setDepartment('');
    setPromotion('');
    if (value.length > 0) {
      setLoading(true);
      setCurrentFunction('getSearch');
      axios.get('/search/' + value + '?page=' + page)
        .then(response => {
          setProducts(response.data || []);
          setPages(response.data.count)
          setErrorMessage('');
          setLoading(false);
        })
        .catch(error => {
          setErrorMessage('Something went wrong getting products');
          setLoading(false);
        });
    } else {
      getProducts(page);
    }
  }

  return (
      <Aux>
        <Cockpit 
          showWhich={showWhich} 
          setShowWhich={setShowWhich} 
          departments={departments} 
          department={department} 
          setDepartment={setDepartment} 
          promotions={promotions} 
          promotion={promotion} 
          setPromotion={setPromotion} 
          changeValue={changeValue} 
          getSearch={getSearch} 
          details={details} 
          pages={pages} 
          page={page} 
          updatePage={updatePage} 
          errorMessage={errorMessage} />
        <Switch>



          <Route render={() => <Products 
            showWhich={showWhich} 
            products={products} 
            department={department}
            promotion={promotion}
            setShowWhich={setShowWhich} 
            loading={loading} 
            setLoading={setLoading} 
            setErrorMessage={(msg) => setErrorMessage(msg)} 
            showWhich={setShowWhich} />} />
        </Switch>
      </Aux>
    );
}

export default ProductsListBuilder;