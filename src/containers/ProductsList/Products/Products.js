import React, { useState, useEffect } from 'react';

import axios from '../../../axios-orders';
import Product from '../../../components/Product/Product';
import Spinner from '../../../components/Spinner/Spinner';
import Aux from '../../../hoc/Aux';

import classes from './Products.module.css';


const Products = (props) => {
  return (
    props.loading ? <Spinner /> : (Object.keys(props.products).length > 0 ?
      <Aux>
        <div className={classes.Card}>
          <div className={classes.Description}>
            <div className={classes.Block}>
              <h3>Name</h3>
            </div>
            <div className={classes.Block}>
              <h3>Price</h3>
            </div>
            <div className={classes.Block}>
              <h3>Department</h3>
            </div>
            <div className={classes.Block}>
              <h3>Promotions</h3>
            </div>
          </div>
        </div>
        {Object.keys(props.products).map(function(key, value) {
        return <Product
          key={key}
          id={props.products[key].id} 
          name={props.products[key].name} 
          price={props.products[key].price} 
          department={props.department.length > 0 ? props.department : props.products[key].department} 
          promotion={props.promotion.length > 0 ? props.promotion : []} 
          promotions={props.products[key].promotions} />
      })}
      </Aux> : 
    <p>This list is empty.</p>)
  );
} 

export default Products;